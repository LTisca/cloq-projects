# coding: utf-8
# usr: Luz Elena Tiscareno Montoya

# Second Test

import pandas as pd

# Import the data and save it
data_superstore = pd.read_csv("Data/Sample_Superstore.csv", encoding="latin1") #The 'latin1' is a character for including many special characters
data_superstore.info() # More information about the data

# Select the columns in which we are interested
selected_columns = ["Order ID", "Order Date", "Ship Date", "Ship Mode", "Postal Code", "Sales", "Quantity", "Discount", "Profit"]
data_selected = data_superstore[selected_columns]

# Consider that the total cost it's the product of "Sales" and "Quantity" and adding the "Total Cost" column into the same table 
data_selected.loc[:, "Total Cost"] = data_selected["Sales"] * data_selected["Quantity"]
data_selected_model = data_selected.copy() # Security copy

# Create a column to see exactly how much time (days) passed between the order day and the shipping day
data_selected_model["Order Date"] = pd.to_datetime(data_selected_model["Order Date"]) # Convert the arguments to datetime
data_selected_model["Ship Date"] = pd.to_datetime(data_selected_model["Ship Date"])
data_selected_model["Shipping Time"] = (data_selected_model["Ship Date"] - data_selected_model["Order Date"]).dt.days # Select only the difference in days

# Create a new column ("More than 4 days") to give values of 0 or 1 in case that take more than 4 days (1) or less than 4 days (0), respectively
def give_value(row):
    if row['Shipping Time'] > 4:
        return 1
    elif row['Shipping Time'] <= 4:
        return 0
data_selected_model.loc[:,'More than 4 days'] = data_selected_model.apply(give_value, axis=1)

# Import the train_test_split function from scikit-learn for splitting the dataset into training and testing sets
from sklearn.model_selection import train_test_split

# Import the RandomForestClassifier from scikit-learn for building a random forest classification model
from sklearn.ensemble import RandomForestClassifier

# Import specific metrics for model evaluation, including accuracy, precision, recall, and ROC AUC score
from sklearn.metrics import accuracy_score, precision_score, recall_score, roc_auc_score

# Select predictor characteristics (predictor variables) and the target variable
features = ["Postal Code","Discount", "Profit", "Total Cost", "More than 4 days"]
target = "Shipping Time"  # Target variable

X = data_selected_model[features] # Extract the features from the dataset for use in model training and testing
y = data_selected_model[target] # Extract the target variable (the variable we want to predict) from the dataset

# Split the data into training and testing sets using the train_test_split function
# X_train and y_train will be used for model training, and X_test and y_test for evaluation
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42) 

# Create a Random Forest Classifier model with a specified random state for reproducibility
clf = RandomForestClassifier(random_state=42)
clf.fit(X_train, y_train) # Fit (train) the Random Forest model using the training data (X_train and y_train)

# Use the trained Random Forest Classifier model to make predictions on the test dataset (X_test)
y_pred = clf.predict(X_test)

# Calculate performance metrics
accuracy = accuracy_score(y_test, y_pred) # Calculate the accuracy score to measure the proportion of correct predictions
precision = precision_score(y_test, y_pred, average="micro") # Calculate the precision score, considering a multiclass problem and using micro-averaging (equal class importance) 
recall = recall_score(y_test, y_pred, average='micro') # Calculate the recall score, considering a multiclass problem and using micro-averaging
roc_auc = roc_auc_score(y_test, clf.predict_proba(X_test), multi_class="ovr") # Calculate the ROC AUC score, considering a multiclass problem and using "ovr" strategy

# Display the metrics
print(f"Accuracy: {accuracy:.2f}")
print(f"Precision: {precision:.2f}")
print(f"Recall: {recall:.2f}")
print(f"ROC AUC: {roc_auc:.2f}")

########### Making an example prediction ###########

# Create a sample order with specific features for prediction
example_order = pd.DataFrame({
    "Postal Code": [92627],
    "Discount": [0.00],
    "Profit": [50.0],
    "Total Cost": [500.0],
    "More than 4 days": [0],
})

# Use the trained model to predict the shipping time for the sample order
prediction = clf.predict(example_order)

# Calculate the probability of the predicted outcome using the model
probability = clf.predict_proba(example_order)[:, 1]

# Check the model's prediction for the sample order and provide an interpretation
if prediction[0] == 1:
    # If the prediction is 1, it means the order will take more than 4 days to be shipped
    print("The order will take more than 4 days to be shipped")
else:
    # If the prediction isn't 1 (likely 0), it means the order will take 4 days or less to be shipped
    print("The order will take 4 days or less to be shipped")
    
# Display the probability of the predicted outcome with two decimal places
print(f"Probabilidad: {probability[0]:.2f}")


